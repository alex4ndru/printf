/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_char.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 13:13:10 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 13:13:12 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		solve_char(t_tags *tags, va_list *arg_list)
{
	char	arg_c;

	arg_c = (char)va_arg(*arg_list, int);
	tags->width--;
	tags->is_prec = 0;
	tags->prec = 0;
	if (tags->is_minus)
	{
		ft_putchar(arg_c);
		tags->print_len += print_argument("", tags, "");
		tags->print_len++;
	}
	else
	{
		tags->print_len += print_argument("", tags, "");
		tags->print_len++;
		ft_putchar(arg_c);
	}
}
