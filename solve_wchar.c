/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_wchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 12:21:37 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 12:21:39 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		solve_wchar(t_tags *tags, va_list *arg_list)
{
	wchar_t				arg_wc;

	arg_wc = (wchar_t)va_arg(*arg_list, wint_t);
	tags->width -= ft_wcharlen(arg_wc);
	tags->prec = 0;
	if (tags->is_minus)
	{
		ft_putwchar(arg_wc);
		tags->print_len += ft_wcharlen(arg_wc);
		tags->print_len += print_argument("", tags, "");
	}
	else
	{
		tags->print_len += print_argument("", tags, "");
		tags->print_len += ft_wcharlen(arg_wc);
		ft_putwchar(arg_wc);
	}
}
