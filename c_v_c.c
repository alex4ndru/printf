/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_v_c.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:26:53 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:26:55 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			c_v_c(const char *str, char *conv, t_tags *tags, va_list *arg_list)
{
	int		i;
	int		j;

	i = -1;
	while (str[++i] != '\0')
	{
		j = -1;
		while (CONVERSION[++j])
		{
			if (str[i] && CONVERSION[j] == str[i])
			{
				if (c_v_f(str, i, tags, arg_list) == -1)
					return (-1);
				*conv = CONVERSION[j];
				return (i);
			}
		}
	}
	return (-1);
}
