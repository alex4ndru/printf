/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_delimiter.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:31:56 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:31:59 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	do_if(int len, char **str, int sign_offset, char *arg_str)
{
	int		j;
	int		counter;

	len += (len - sign_offset) / 3;
	*str = ft_strnew(len + 1);
	(*str)[len] = '\0';
	if (sign_offset == 1)
		(*str)[0] = arg_str[0];
	j = ft_strlen(arg_str) - 1;
	len--;
	counter = 0;
	while (len >= sign_offset)
	{
		(*str)[len] = arg_str[j];
		counter++;
		if (counter % 3 == 0)
		{
			len--;
			(*str)[len] = ',';
		}
		j--;
		len--;
	}
}

char		*get_delimiter(char *arg_str)
{
	char	*str;
	int		len;
	int		sign_offset;

	len = ft_strlen(arg_str);
	sign_offset = 0;
	if (arg_str[0] == '-' || arg_str[0] == '+')
		sign_offset = 1;
	if (len - sign_offset > 3)
		do_if(len, &str, sign_offset, arg_str);
	else
		return (arg_str);
	free(arg_str);
	return (str);
}
