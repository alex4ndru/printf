/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_with_prec_wstr.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:33:43 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:33:44 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		print_with_prec_wstr(wchar_t *str, int prec)
{
	int		len;
	int		t;
	int		i;

	len = 0;
	i = -1;
	while (str[++i])
	{
		t = ft_wcharlen(str[i]);
		prec = prec - t;
		if (prec >= 0)
		{
			len += t;
			ft_putwchar(str[i]);
		}
		else
			break ;
	}
	return (len);
}
