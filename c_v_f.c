/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_v_f.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:27:16 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:27:18 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	check_d(t_flags *fl)
{
	if (fl->str[*fl->i] == '.')
	{
		*fl->i += 1;
		fl->tags->is_prec = 1;
		if (fl->str[*fl->i] >= '0' && fl->str[*fl->i] <= '9')
		{
			fl->tags->prec = ft_atoi(fl->str + *fl->i);
			while (fl->str[*fl->i] >= '0' && fl->str[*fl->i] <= '9')
				*fl->i += 1;
		}
		else if (fl->str[*fl->i] == '*')
		{
			fl->tags->prec = va_arg(*fl->arg_list, int);
			if (fl->tags->prec < 0)
			{
				fl->tags->is_prec = 0;
				fl->tags->prec = 0;
			}
			*fl->i += 1;
		}
	}
	else
		check_e(fl);
}

static void	check_c(t_flags *fl)
{
	if (fl->str[*fl->i] == '*')
	{
		fl->tags->width = va_arg(*fl->arg_list, int);
		if (fl->tags->width < 0)
		{
			fl->tags->is_minus = 1;
			fl->tags->width *= -1;
		}
		*fl->i += 1;
	}
	else
		check_d(fl);
}

static void	check_b(t_flags *fl)
{
	if (fl->str[*fl->i] == ' ')
	{
		fl->tags->is_space = 1;
		*fl->i += 1;
	}
	else if (fl->str[*fl->i] == '\'')
	{
		fl->tags->is_delim = 1;
		*fl->i += 1;
	}
	else if (fl->str[*fl->i] >= '1' && fl->str[*fl->i] <= '9')
	{
		fl->tags->width = ft_atoi(fl->str + *fl->i);
		while (fl->str[*fl->i] >= '0' && fl->str[*fl->i] <= '9')
			*fl->i += 1;
	}
	else
		check_c(fl);
}

static void	check_a(t_flags *fl)
{
	if (fl->str[*fl->i] == '#')
	{
		fl->tags->is_diez = 1;
		*fl->i += 1;
	}
	else if (fl->str[*fl->i] == '0')
	{
		fl->tags->is_zero = 1;
		*fl->i += 1;
	}
	else if (fl->str[*fl->i] == '-')
	{
		fl->tags->is_minus = 1;
		*fl->i += 1;
	}
	else if (fl->str[*fl->i] == '+')
	{
		fl->tags->is_plus = 1;
		*fl->i += 1;
	}
	else
		check_b(fl);
}

int			c_v_f(const char *str, int limit, t_tags *tags, va_list *arg_list)
{
	int		i;
	t_flags	fl;

	fl.i = &i;
	fl.str = str;
	fl.limit = limit;
	fl.tags = tags;
	fl.arg_list = arg_list;
	i = 0;
	while (i < limit)
	{
		check_a(&fl);
		if (i == -1)
			return (-1);
	}
	return (i);
}
