/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 17:20:15 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/04 17:20:17 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_nr_len(int nr, int base)
{
	long	nr_long;
	int		len;

	nr_long = nr;
	if (nr < 0)
		nr_long = -nr_long;
	len = 1;
	while ((nr_long = nr_long / base))
		len++;
	return (len);
}

static char	*solve_it(long nr_long, int sign_offset, int base, int high)
{
	char	*nr_str;
	long	t;
	int		i;
	int		nr_len;
	int		letter;

	letter = high == 0 ? 87 : 55;
	nr_len = ft_nr_len(nr_long, base);
	nr_str = (char*)malloc(sizeof(char) * (nr_len + sign_offset + 1));
	nr_str[nr_len] = '\0';
	if (sign_offset == 1)
		nr_str[0] = '-';
	i = nr_len - 1;
	while (i >= sign_offset)
	{
		t = nr_long % base;
		if (t <= 9)
			nr_str[i] = t + '0';
		else
			nr_str[i] = t + letter;
		nr_long = nr_long / base;
		i--;
	}
	return (nr_str);
}

char		*ft_itoa_base(int nr, int base, int high)
{
	char	*nr_str;
	int		sign_offset;
	long	nr_long;
	int		nr_len;

	nr_len = ft_nr_len(nr, base);
	nr_long = nr;
	if (base != 10 && nr_long < 0)
		nr_long = -nr_long;
	sign_offset = 0;
	if (nr < 0)
	{
		sign_offset = 1;
		nr_long = -nr_long;
	}
	nr_str = solve_it(nr_long, sign_offset, base, high);
	return (nr_str);
}
