/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_litoa_base.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 13:29:03 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/06 13:29:05 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_nr_len(long long nr, int base)
{
	int		len;

	if (nr == -9223372036854775807 - 1)
		return (19);
	if (nr < 0)
		nr = -nr;
	len = 1;
	while ((nr = nr / base))
		len++;
	return (len);
}

static char	*solve_it(long long nr, int sign_offset, int base, int high)
{
	char	*nr_str;
	long	t;
	int		i;
	int		nr_len;
	int		letter;

	letter = high == 0 ? 87 : 55;
	nr_len = ft_nr_len(nr, base) + sign_offset;
	nr_str = (char*)malloc(sizeof(char) * (nr_len + 1));
	nr_str[nr_len] = '\0';
	if (sign_offset == 1)
		nr_str[0] = '-';
	i = nr_len - 1;
	while (i >= sign_offset)
	{
		t = nr % base;
		if (t <= 9)
			nr_str[i] = t + '0';
		else
			nr_str[i] = t + letter;
		nr = nr / base;
		i--;
	}
	return (nr_str);
}

char		*ft_litoa_base(long long nr, int base, int high)
{
	char	*nr_str;
	int		sign_offset;
	int		nr_len;

	if (nr == -9223372036854775807 - 1)
		return (ft_strdup("-9223372036854775808"));
	nr_len = ft_nr_len(nr, base);
	if (base != 10 && nr < 0)
		nr = -nr;
	sign_offset = 0;
	if (nr < 0)
	{
		sign_offset = 1;
		nr = -nr;
	}
	nr_str = solve_it(nr, sign_offset, base, high);
	return (nr_str);
}
