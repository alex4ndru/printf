/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcharlen.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 13:12:04 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/10 13:12:07 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_wcharlen(wchar_t wc)
{
	int		len;

	len = 0;
	if (wc <= 127)
		len = 1;
	else if (wc <= 0x7FF)
		len = 2;
	else if (wc <= 0xFFFF)
		len = 3;
	else if (wc <= 0x10FFFF)
		len = 4;
	return (len);
}
