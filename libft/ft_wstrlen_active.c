/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrlen_active.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 13:12:17 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/10 13:12:21 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_wstrlen_active(wchar_t *wstr)
{
	int		len;
	int		i;

	len = 0;
	i = -1;
	while (wstr[++i])
		len += ft_wcharlen(wstr[i]);
	return (len);
}
