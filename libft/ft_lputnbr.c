/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lputnbr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/10 12:10:35 by ialexand          #+#    #+#             */
/*   Updated: 2017/12/10 12:26:10 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

static void	put_it(long long n)
{
	if (n >= 10)
		put_it(n / 10);
	ft_putchar(n % 10 + '0');
}

void		ft_lputnbr(long long n)
{
	if (n == (-9223372036854775807 - 1))
		ft_putstr("-9223372036854775808");
	else
	{
		if (n < 0)
		{
			ft_putchar('-');
			n = -n;
		}
		put_it(n);
	}
}
