/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtoupper.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 14:56:01 by ialexand          #+#    #+#             */
/*   Updated: 2017/12/15 15:44:02 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char	*ft_strtoupper(char *str)
{
	size_t	i;
	char	*result;

	result = ft_strdup(str);
	i = 0;
	while (result[i] != '\0')
	{
		result[i] = ft_toupper(result[i]);
		i++;
	}
	return (result);
}
