/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 13:11:25 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/10 13:11:32 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	do_case1(uint8_t *bytes, int *num_bytes)
{
	*bytes = 0x00;
	*num_bytes = 1;
}

static void	do_case2(uint8_t *bytes, int *num_bytes)
{
	*bytes = 0xC0;
	*num_bytes = 2;
}

static void	do_case3(uint8_t *bytes, int *num_bytes)
{
	*bytes = 0xE0;
	*num_bytes = 3;
}

static void	do_case4(uint8_t *bytes, int *num_bytes)
{
	*bytes = 0xF0;
	*num_bytes = 4;
}

void		ft_putwchar(wchar_t x)
{
	uint32_t	wc;
	uint8_t		bytes[4];
	int			num_bytes;
	int			i;

	num_bytes = 0;
	wc = (uint32_t)x;
	if (wc <= 127)
		do_case1(&bytes[0], &num_bytes);
	else if (wc <= 0x7FF)
		do_case2(&bytes[0], &num_bytes);
	else if (wc <= 0xFFFF)
		do_case3(&bytes[0], &num_bytes);
	else if (wc <= 0x10FFFF)
		do_case4(&bytes[0], &num_bytes);
	i = num_bytes;
	while (--i > 0)
	{
		bytes[i] = 0x80 | (uint8_t)(wc & 0x3F);
		wc >>= 6;
	}
	bytes[0] |= (uint8_t)wc;
	i = -1;
	while (++i < num_bytes)
		ft_putchar(bytes[i]);
}
