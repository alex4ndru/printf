/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_luitoa_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 17:20:15 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/04 17:20:17 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_nr_len(unsigned long long nr, int base)
{
	int		len;

	len = 1;
	while ((nr = nr / base))
		len++;
	return (len);
}

char		*ft_luitoa_base(unsigned long long nr, int base, int high)
{
	char	*nr_str;
	long	t;
	int		i;
	int		nr_len;
	int		letter;

	letter = high == 0 ? 87 : 55;
	nr_len = ft_nr_len(nr, base);
	nr_str = (char*)malloc(sizeof(char) * (nr_len + 1));
	nr_str[nr_len] = '\0';
	i = nr_len - 1;
	while (i >= 0)
	{
		t = nr % base;
		if (t <= 9)
			nr_str[i] = t + '0';
		else
			nr_str[i] = t + letter;
		nr = nr / base;
		i--;
	}
	return (nr_str);
}
