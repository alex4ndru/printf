/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_pointer.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 14:31:25 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 14:31:28 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		solve_pointer(t_tags *tags, va_list *arg_list)
{
	uintptr_t	arg_p;
	char		*s;

	arg_p = va_arg(*arg_list, uintptr_t);
	if (tags->is_prec && tags->prec == 0)
		s = ft_strdup("");
	else
		s = ft_luitoa_base((unsigned long long)arg_p, 16, 0);
	tags->print_len += print_argument(s, tags, "0x");
	free(s);
}
