/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_int.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 13:28:21 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 13:28:23 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	do_print(t_tags *tags, long long arg_i, char *s)
{
	if (tags->is_prec && tags->prec == 0 && arg_i == 0)
		tags->print_len += print_argument("", tags, "");
	else if (arg_i < 0)
		tags->print_len += print_argument(s + 1, tags, "-");
	else if (tags->is_plus)
		tags->print_len += print_argument(s, tags, "+");
	else if (tags->is_space)
		tags->print_len += print_argument(s, tags, " ");
	else
		tags->print_len += print_argument(s, tags, "");
}

void		solve_int(t_tags *tags, va_list *arg_list, char conversion)
{
	long long	arg_i;
	char		*s;

	if (tags->tag_long == 1 || conversion == 'D')
		arg_i = va_arg(*arg_list, long int);
	else if (tags->tag_long == 2)
		arg_i = va_arg(*arg_list, long long int);
	else if (tags->tag_short == 1)
		arg_i = (short)va_arg(*arg_list, int);
	else if (tags->tag_short == 2)
		arg_i = (char)va_arg(*arg_list, int);
	else if (tags->tag_max == 1)
		arg_i = va_arg(*arg_list, intmax_t);
	else if (tags->tag_size_t == 1)
		arg_i = va_arg(*arg_list, size_t);
	else
		arg_i = va_arg(*arg_list, int);
	s = ft_litoa_base(arg_i, 10, 0);
	if (tags->prec > 0)
		tags->is_zero = 0;
	if (tags->is_delim)
		s = get_delimiter(s);
	do_print(tags, arg_i, s);
	free(s);
}
