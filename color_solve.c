/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_solve.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:27:27 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:27:29 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		color_solve(int color_id)
{
	if (color_id == -1)
		ft_putstr(C_RESET);
	else if (color_id == -2)
		ft_putstr(B_RESET);
	else if (color_id == 1)
		ft_putstr(C_RED);
	else if (color_id == 2)
		ft_putstr(C_GREEN);
	else if (color_id == 3)
		ft_putstr(C_YELLOW);
	else if (color_id == 4)
		ft_putstr(C_BLUE);
	else if (color_id == 5)
		ft_putstr(C_MAGENTA);
	else if (color_id == 6)
		ft_putstr(C_CYAN);
	else if (color_id == 7)
		ft_putstr(C_BLACK);
	else if (color_id == 8)
		ft_putstr(B_YELLOW);
	else if (color_id == 9)
		ft_putstr(B_BLUE);
}
