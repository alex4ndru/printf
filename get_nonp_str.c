/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_nonp_str.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:32:07 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:32:09 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	do_while(char **str_p, char **str, int i, int j)
{
	int		code;

	code = (*str_p)[i];
	if (code >= 0 && code <= 31)
	{
		(*str)[j] = '^';
		(*str)[j + 1] = '@' + code;
		j += 2;
	}
	else if (code == 127)
	{
		(*str)[j] = '^';
		(*str)[j + 1] = '?';
		j += 2;
	}
	else
	{
		(*str)[j] = code;
		j++;
	}
}

void		get_nonp_str(char **str_p)
{
	int		i;
	int		j;
	int		len;
	char	*str;

	len = ft_strlen(*str_p) * 2 + 2;
	str = ft_strnew(len + 1);
	j = 0;
	i = -1;
	while (i == -1 || (*str_p)[++i - 1])
		do_while(str_p, &str, i, j);
	free(*str_p);
	*str_p = ft_strnew(j);
	ft_strcpy(*str_p, str);
	free(str);
}
