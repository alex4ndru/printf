/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_percent.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 14:21:49 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 14:21:50 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		solve_percent(t_tags *tags)
{
	char	*s;

	s = ft_strdup("%");
	tags->is_prec = 0;
	tags->prec = 0;
	tags->print_len += print_argument(s, tags, "");
	free(s);
}
