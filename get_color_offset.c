/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_color_offset.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:31:47 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:31:48 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	register_color(int color_id, int offset, t_tags *tags)
{
	tags->color_id = color_id;
	if (color_id > 0 && color_id <= 7)
		tags->is_text_color = 1;
	else if (color_id > 7)
		tags->is_bg_color = 1;
	else if (color_id == -1)
		tags->is_text_color = 0;
	else if (color_id == -2)
		tags->is_bg_color = 0;
	return (offset);
}

int			get_color_offset(const char *str, t_tags *tags)
{
	tags->color_id = 0;
	if (!ft_strncmp(str, "{ecolor}", 8))
		return (register_color(-1, 8, tags));
	else if (!ft_strncmp(str, "{red}", 5))
		return (register_color(1, 5, tags));
	else if (!ft_strncmp(str, "{green}", 7))
		return (register_color(2, 7, tags));
	else if (!ft_strncmp(str, "{yellow}", 8))
		return (register_color(3, 8, tags));
	else if (!ft_strncmp(str, "{blue}", 6))
		return (register_color(4, 6, tags));
	else if (!ft_strncmp(str, "{magenta}", 9))
		return (register_color(5, 9, tags));
	else if (!ft_strncmp(str, "{cyan}", 6))
		return (register_color(6, 6, tags));
	else if (!ft_strncmp(str, "{black}", 7))
		return (register_color(7, 7, tags));
	else if (!ft_strncmp(str, "{b_ecolor}", 10))
		return (register_color(-2, 10, tags));
	else if (!ft_strncmp(str, "{b_yellow}", 10))
		return (register_color(8, 10, tags));
	else if (!ft_strncmp(str, "{b_blue}", 8))
		return (register_color(9, 8, tags));
	return (0);
}
