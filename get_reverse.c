/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_reverse.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:32:56 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:32:57 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*get_reverse(char *arg_str)
{
	char	*str;
	int		len;
	char	t;
	int		i;

	if (arg_str == NULL)
		return (ft_strdup("(null)"));
	str = ft_strdup(arg_str);
	len = ft_strlen(str);
	i = -1;
	while (++i < (len / 2))
	{
		t = str[i];
		str[i] = str[len - 1 - i];
		str[len - 1 - i] = t;
	}
	return (str);
}
