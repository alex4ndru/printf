/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_e.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 11:56:49 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 11:56:52 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	check_e(t_flags *fl)
{
	if (fl->str[*fl->i] == 'j' || fl->str[*fl->i] == 'z' ||
		fl->str[*fl->i] == 'h' || fl->str[*fl->i] == 'l')
	{
		length_update(fl->tags, fl->str[*fl->i]);
		if (fl->str[*fl->i + 1] && fl->str[*fl->i + 1] == fl->str[*fl->i] &&
			(fl->str[*fl->i] == 'l' || fl->str[*fl->i] == 'h'))
		{
			length_update(fl->tags, fl->str[*fl->i + 1]);
			*fl->i += 1;
		}
		*fl->i += 1;
	}
	else
		*fl->i = -1;
}
