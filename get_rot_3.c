/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_rot_3.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:32:33 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:32:34 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*get_rot_3(char *arg_str)
{
	char	*letters_l;
	char	*letters_h;
	char	*digits;
	char	*str;
	int		i;

	letters_l = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
	letters_h = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
	digits = "012345678900123456789001234567890";
	if (arg_str == NULL)
		return (ft_strdup("(null)"));
	str = ft_strdup(arg_str);
	i = -1;
	while (str[++i] != '\0')
	{
		if (str[i] >= 'a' && str[i] <= 'z')
			str[i] = letters_l[str[i] - 'a' + 3];
		else if (str[i] >= 'A' && str[i] <= 'Z')
			str[i] = letters_h[str[i] - 'A' + 3];
		else if (str[i] >= '0' && str[i] <= '9')
			str[i] = digits[str[i] - '0' + 3];
	}
	return (str);
}
