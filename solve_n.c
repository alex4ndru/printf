/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_n.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 14:36:17 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 14:36:18 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		solve_n(t_tags *tags, va_list *arg_list)
{
	if (tags->tag_short == 2)
		*va_arg(*arg_list, char*) = tags->print_len;
	else if (tags->tag_short == 1)
		*va_arg(*arg_list, short*) = tags->print_len;
	else if (tags->tag_long == 1)
		*va_arg(*arg_list, long*) = tags->print_len;
	else if (tags->tag_long == 2)
		*va_arg(*arg_list, long long*) = tags->print_len;
	else if (tags->tag_max == 1)
		*va_arg(*arg_list, intmax_t*) = tags->print_len;
	else if (tags->tag_size_t == 1)
		*va_arg(*arg_list, size_t*) = tags->print_len;
	else
		*va_arg(*arg_list, int*) = tags->print_len;
}
