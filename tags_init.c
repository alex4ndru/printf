/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tags_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:33:55 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:33:56 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		tags_init(t_tags *tags)
{
	tags->tag_short = 0;
	tags->tag_long = 0;
	tags->tag_max = 0;
	tags->tag_size_t = 0;
	tags->width = 0;
	tags->prec = 0;
	tags->is_prec = 0;
	tags->is_diez = 0;
	tags->is_zero = 0;
	tags->is_minus = 0;
	tags->is_plus = 0;
	tags->is_space = 0;
	tags->is_delim = 0;
	tags->color_id = 0;
}
