/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_argument.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:33:33 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:33:35 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		len_with_precision(char *str, int prec)
{
	int		len;

	len = prec - ft_strlen(str);
	if (len > 0)
		return (prec);
	else
		return (ft_strlen(str));
}

static void		print_with_precision(char *str, int prec)
{
	int		len;

	len = prec - ft_strlen(str);
	if (len > 0)
	{
		ft_put_padding(len, '0');
		ft_putstr(str);
	}
	else
		ft_putstr(str);
}

static void		put_zero(char *prefix, int len, char *str, int prec)
{
	ft_putstr(prefix);
	ft_put_padding(len, '0');
	print_with_precision(str, prec);
}

static void		put_not_zero(char *prefix, int len, char *str, int prec)
{
	ft_put_padding(len, ' ');
	ft_putstr(prefix);
	print_with_precision(str, prec);
}

int				print_argument(char *str, t_tags *tags, char *prefix)
{
	int		len;

	len = tags->width - len_with_precision(str, tags->prec) - ft_strlen(prefix);
	if (len > 0)
	{
		if (tags->is_minus)
		{
			ft_putstr(prefix);
			print_with_precision(str, tags->prec);
			ft_put_padding(len, ' ');
		}
		else
		{
			if (tags->is_zero)
				put_zero(prefix, len, str, tags->prec);
			else
				put_not_zero(prefix, len, str, tags->prec);
		}
		return (tags->width);
	}
	ft_putstr(prefix);
	print_with_precision(str, tags->prec);
	return (ft_strlen(prefix) + len_with_precision(str, tags->prec));
}
