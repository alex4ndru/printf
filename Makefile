# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ialexand <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/12 10:33:14 by ialexand          #+#    #+#              #
#    Updated: 2018/03/12 10:33:15 by ialexand         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= libftprintf.a
CFLAGS	= -Werror -Wall -Wextra

SRC		= 	c_v_c.c \
			c_v_f.c \
			check_e.c \
			color_solve.c \
			conversion_solve.c \
			ft_printf.c \
			get_color_offset.c \
			get_delimiter.c \
			get_nonp_str.c \
			get_reverse.c \
			get_rot_3.c \
			get_rot_13.c \
			length_update.c \
			print_argument_wstr.c \
			print_argument.c \
			print_with_prec_wstr.c \
			solve_char.c \
			solve_else.c \
			solve_int.c \
			solve_n.c \
			solve_percent.c \
			solve_pointer.c \
			solve_r.c \
			solve_string.c \
			solve_string2.c \
			solve_unsigned.c \
			solve_wchar.c \
			solve_wstring.c \
			tags_init.c \

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME):
		@make -C ./libft
		@gcc $(CFLAGS) -I./includes -I./libft/includes -c $(SRC)
		@cp libft/libft.a ./$(NAME)
		@ar rc $(NAME) $(OBJ)
		@ranlib $(NAME)
clean:
		@make clean -C ./libft
		@/bin/rm -f $(OBJ)

fclean: clean
		@make fclean -C ./libft
		@/bin/rm -f $(NAME)
re: fclean all
