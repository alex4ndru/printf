/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_r.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 14:38:26 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 14:38:28 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		solve_r(t_tags *tags, va_list *arg_list)
{
	char	*arg_str;
	char	*s;

	arg_str = va_arg(*arg_list, char*);
	if (arg_str == NULL)
		s = ft_strdup("(null)");
	else
		s = ft_strdup(arg_str);
	if (tags->is_prec && tags->prec < (int)ft_strlen(s))
		s[tags->prec] = '\0';
	get_nonp_str(&s);
	tags->is_prec = 0;
	tags->prec = 0;
	tags->print_len += print_argument(s, tags, "");
	free(s);
}
