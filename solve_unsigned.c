/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_unsigned.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 14:01:40 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 14:01:43 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	check_u_b(t_tags *t, char cn, unsigned long long au, char **s)
{
	if (cn == 'x')
	{
		*s = ft_luitoa_base(au, 16, 0);
		if (t->is_prec && t->prec == 0 && au == 0)
			t->print_len += print_argument("", t, "");
		else if (t->is_diez && (t->prec <= (int)ft_strlen(*s)) - 2 && au != 0)
			t->print_len += print_argument(*s, t, "0x");
		else
			t->print_len += print_argument(*s, t, "");
	}
	else if (cn == 'X')
	{
		*s = ft_luitoa_base(au, 16, 1);
		if (t->is_prec && t->prec == 0 && au == 0)
			t->print_len += print_argument("", t, "");
		else if (t->is_diez && (t->prec <= (int)ft_strlen(*s)) - 2 && au != 0)
			t->print_len += print_argument(*s, t, "0X");
		else
			t->print_len += print_argument(*s, t, "");
	}
	else
	{
		*s = ft_luitoa_base(au, 2, 0);
		t->print_len += print_argument(*s, t, "");
	}
}

static void	check_u_a(t_tags *tags, char cn, unsigned long long au, char **s)
{
	if (cn == 'u' || cn == 'U')
	{
		*s = ft_luitoa_base(au, 10, 0);
		if (tags->is_prec && tags->prec == 0 && au == 0)
			tags->print_len += print_argument("", tags, "");
		else
			tags->print_len += print_argument(*s, tags, "");
	}
	else if (cn == 'o' || cn == 'O')
	{
		*s = ft_luitoa_base(au, 8, 0);
		if (tags->is_prec && tags->prec == 0 && au == 0 && tags->is_diez == 0)
			tags->print_len += print_argument("", tags, "");
		else if (tags->is_diez && (tags->prec <= (int)ft_strlen(*s)) && au != 0)
			tags->print_len += print_argument(*s, tags, "0");
		else
			tags->print_len += print_argument(*s, tags, "");
	}
	else
		check_u_b(tags, cn, au, s);
}

void		solve_unsigned(t_tags *tags, va_list *arg_list, char conversion)
{
	unsigned long long	au;
	char				*s;

	if (tags->tag_long == 1 || conversion == 'U' || conversion == 'O')
		au = va_arg(*arg_list, unsigned long int);
	else if (tags->tag_long == 2)
		au = va_arg(*arg_list, unsigned long long int);
	else if (tags->tag_short == 1)
		au = (unsigned short)va_arg(*arg_list, unsigned int);
	else if (tags->tag_short == 2)
		au = (unsigned char)va_arg(*arg_list, unsigned int);
	else if (tags->tag_max == 1)
		au = va_arg(*arg_list, uintmax_t);
	else if (tags->tag_size_t == 1)
		au = va_arg(*arg_list, size_t);
	else
		au = va_arg(*arg_list, unsigned int);
	if (tags->prec > 0)
		tags->is_zero = 0;
	check_u_a(tags, conversion, au, &s);
	free(s);
}
