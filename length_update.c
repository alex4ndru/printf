/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   length_update.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:33:04 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:33:05 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		length_update(t_tags *tags, char symbol)
{
	if (symbol == 'h')
		tags->tag_short++;
	else if (symbol == 'l')
		tags->tag_long++;
	else if (symbol == 'j')
		tags->tag_max = 1;
	else if (symbol == 'z')
		tags->tag_size_t = 1;
}
