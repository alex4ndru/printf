/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversion_solve.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:27:50 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:27:52 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		part_b(char cn, va_list *arg_list, t_tags *tags, char **arg_str)
{
	if (cn == 'R')
	{
		*arg_str = va_arg(*arg_list, char*);
		*arg_str = get_reverse(*arg_str);
		solve_string2(*arg_str, tags);
		free(*arg_str);
	}
	else if (cn == 'c')
		solve_char(tags, arg_list);
	else if (cn == 'i' || cn == 'd' || cn == 'D')
		solve_int(tags, arg_list, cn);
	else if (cn == 'u' || cn == 'U' || cn == 'x' || cn == 'X' || cn == 'o' ||
			cn == 'O' || cn == 'b')
		solve_unsigned(tags, arg_list, cn);
	else if (cn == '%')
		solve_percent(tags);
	else if (cn == 'p')
		solve_pointer(tags, arg_list);
	else if (cn == 'n')
		solve_n(tags, arg_list);
	else if (cn == 'r')
		solve_r(tags, arg_list);
	else
		solve_else(tags, cn);
}

void			conversion_solve(char cn, va_list *arg_list, t_tags *tags)
{
	char				*arg_str;

	if (cn == 'S' || (cn == 's' && tags->tag_long == 1))
		solve_wstring(tags, arg_list);
	else if (cn == 'C' || (cn == 'c' && tags->tag_long == 1))
		solve_wchar(tags, arg_list);
	else if (cn == 's')
		solve_string(tags, arg_list);
	else if (cn == 'Y' || (cn == 'y' && tags->tag_long == 1))
	{
		arg_str = va_arg(*arg_list, char*);
		arg_str = get_rot_13(arg_str);
		solve_string2(arg_str, tags);
		free(arg_str);
	}
	else if (cn == 'y')
	{
		arg_str = va_arg(*arg_list, char*);
		arg_str = get_rot_3(arg_str);
		solve_string2(arg_str, tags);
		free(arg_str);
	}
	else
		part_b(cn, arg_list, tags, &arg_str);
}
