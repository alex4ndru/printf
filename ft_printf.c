/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:27:38 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:27:40 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	do_loop(const char *f, t_tags *tags, va_list *arg_list, int *i)
{
	int		itf;
	int		color_offset;

	while ((color_offset = get_color_offset(f + *i, tags)))
	{
		*i += color_offset;
		if (color_offset != 0)
			color_solve(tags->color_id);
	}
	if (f[*i] == '%')
	{
		tags_init(tags);
		if ((itf = c_v_c(f + *i + 1, &tags->conversion, tags, arg_list)) != -1)
		{
			conversion_solve(tags->conversion, arg_list, tags);
			*i += itf + 1;
		}
	}
	else
	{
		tags->print_len++;
		write(1, &f[*i], 1);
	}
}

int			ft_printf(const char *format, ...)
{
	va_list		arg_list;
	int			i;
	t_tags		tags;

	va_start(arg_list, format);
	tags.print_len = 0;
	tags.is_text_color = 0;
	tags.is_bg_color = 0;
	i = -1;
	while (format[++i])
		do_loop(format, &tags, &arg_list, &i);
	if (tags.is_text_color == 1)
		color_solve(-1);
	if (tags.is_bg_color == 1)
		color_solve(-2);
	va_end(arg_list);
	return (tags.print_len);
}
