/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_wstring.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 12:12:43 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 12:12:44 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		solve_wstring(t_tags *tags, va_list *arg_list)
{
	wchar_t				*arg_wstr;
	char				*s;

	arg_wstr = va_arg(*arg_list, wchar_t*);
	if (arg_wstr == NULL)
	{
		s = ft_strdup("(null)");
		if (tags->is_prec && tags->prec < (int)ft_strlen(s))
			s[tags->prec] = '\0';
		tags->is_prec = 0;
		tags->prec = 0;
		tags->print_len += print_argument(s, tags, "");
		free(s);
	}
	else
		tags->print_len += print_argument_wstr(arg_wstr, tags);
}
