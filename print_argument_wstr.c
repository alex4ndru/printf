/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_argument_wstr.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:33:27 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:33:28 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	get_len_after_prec(wchar_t *str, int prec)
{
	int		len;
	int		t;
	int		i;

	len = 0;
	i = -1;
	while (str[++i])
	{
		t = ft_wcharlen(str[i]);
		prec = prec - t;
		if (prec >= 0)
			len += t;
		else
			break ;
	}
	return (len);
}

int			print_argument_wstr(wchar_t *wstr, t_tags *tags)
{
	int		len;
	int		prec_length;

	prec_length = ft_wstrlen_active(wstr);
	if (!tags->is_prec || tags->prec > prec_length)
		tags->prec = prec_length;
	len = get_len_after_prec(wstr, tags->prec);
	if (tags->width > len)
	{
		len = tags->width - len;
		if (tags->is_minus)
		{
			print_with_prec_wstr(wstr, tags->prec);
			ft_put_padding(len, ' ');
		}
		else
		{
			ft_put_padding(len, tags->is_zero ? '0' : ' ');
			print_with_prec_wstr(wstr, tags->prec);
		}
		return (tags->width);
	}
	print_with_prec_wstr(wstr, tags->prec);
	return (len);
}
