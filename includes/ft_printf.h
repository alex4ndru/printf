/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:21:21 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:21:24 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <libft.h>
# include <inttypes.h>

# include <stdio.h>

# define C_BLACK	"\x1b[30m"
# define C_RED		"\x1b[31m"
# define C_GREEN	"\x1b[32m"
# define C_YELLOW	"\x1b[33m"
# define C_BLUE		"\x1b[34m"
# define C_MAGENTA	"\x1b[35m"
# define C_CYAN		"\x1b[36m"
# define C_RESET	"\x1b[39m"

# define B_YELLOW	"\e[43m"
# define B_BLUE		"\e[44m"
# define B_RESET	"\e[49m"
# define B_DGREY	"\e[100m"

# define CONVERSION  "idouxXDOUcCsSp%bnryYR"
# define FLAGS "#0-+ "

typedef struct	s_tags
{
	int		tag_short;
	int		tag_long;
	int		tag_max;
	int		tag_size_t;
	int		width;
	int		prec;
	int		is_prec;
	int		is_diez;
	int		is_zero;
	int		is_minus;
	int		is_plus;
	int		is_space;
	int		print_len;
	int		is_delim;
	int		color_id;
	int		is_text_color;
	int		is_bg_color;
	char	conversion;
}				t_tags;

typedef struct	s_flags
{
	int			*i;
	const char	*str;
	int			limit;
	t_tags		*tags;
	va_list		*arg_list;
}				t_flags;

int				ft_printf(const char *format, ...);
int				c_v_c(const char *str, char *cv, t_tags *ts, va_list *a_l);
int				c_v_f(const char *str, int lt, t_tags *ts, va_list *a_l);
void			conversion_solve(char cn, va_list *arg_list, t_tags *tags);
void			tags_init(t_tags *tags);
void			length_update(t_tags *tags, char symbol);
int				print_argument(char *str, t_tags *tags, char *prefix);
int				print_with_prec_wstr(wchar_t *str, int prec);
int				print_argument_wstr(wchar_t *str, t_tags *tags);
void			get_nonp_str(char **str_p);
char			*get_rot_3(char *arg_str);
char			*get_rot_13(char *arg_str);
char			*get_reverse(char *arg_str);
char			*get_delimiter(char *arg_str);
int				get_color_offset(const char *str, t_tags *tags);
void			color_solve(int color_id);
void			check_e(t_flags	*fl);
void			solve_wstring(t_tags *tags, va_list *arg_list);
void			solve_wchar(t_tags *tags, va_list *arg_list);
void			solve_string(t_tags *tags, va_list *arg_list);
void			solve_string2(char *arg_str, t_tags *tags);
void			solve_char(t_tags *tags, va_list *arg_list);
void			solve_int(t_tags *tags, va_list *arg_list, char conversion);
void			solve_unsigned(t_tags *tags, va_list *arg_list, char conv);
void			solve_percent(t_tags *tags);
void			solve_pointer(t_tags *tags, va_list *arg_list);
void			solve_n(t_tags *tags, va_list *arg_list);
void			solve_r(t_tags *tags, va_list *arg_list);
void			solve_else(t_tags *tags, char arg_c);
#endif
