/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_else.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 14:45:12 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 14:45:13 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		solve_else(t_tags *tags, char arg_c)
{
	tags->width--;
	tags->is_prec = 0;
	tags->prec = 0;
	if (tags->is_minus)
	{
		ft_putchar(arg_c);
		tags->print_len += print_argument("", tags, "");
		tags->print_len++;
	}
	else
	{
		tags->print_len += print_argument("", tags, "");
		tags->print_len++;
		ft_putchar(arg_c);
	}
}
