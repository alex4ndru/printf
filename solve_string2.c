/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_string2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 10:33:49 by ialexand          #+#    #+#             */
/*   Updated: 2018/03/12 10:33:50 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		solve_string2(char *arg_str, t_tags *tags)
{
	char	*s;

	if (arg_str == NULL)
		s = ft_strdup("(null)");
	else
		s = ft_strdup(arg_str);
	if (tags->is_prec && tags->prec < (int)ft_strlen(s))
		s[tags->prec] = '\0';
	tags->is_prec = 0;
	tags->prec = 0;
	tags->print_len += print_argument(s, tags, "");
	free(s);
}
